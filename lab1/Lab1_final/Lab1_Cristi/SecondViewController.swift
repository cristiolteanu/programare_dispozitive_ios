//
//  SecondViewController.swift
//  Lab1_Cristi
//
//  Created by Cristian Olteanu on 12/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    var name: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        label.text = "Hello " + name
    }

    @IBAction func pressedClose(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
