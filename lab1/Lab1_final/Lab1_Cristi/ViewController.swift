//
//  ViewController.swift
//  Lab1_Cristi
//
//  Created by Cristian Olteanu on 12/03/2019.
//  Copyright © 2019 none. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func pressedButton(_ sender: Any) {
        if textField.text!.isEmpty {
            var alert: UIAlertController = UIAlertController(title: "Alert", message: "Text field is empty", preferredStyle: .alert)
            var cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        } else {
            let second = storyboard?.instantiateViewController(withIdentifier: "second") as! SecondViewController
            second.name = textField.text
            present(second, animated: true, completion: nil)
        }
    }

}

