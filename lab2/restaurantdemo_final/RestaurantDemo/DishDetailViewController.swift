//
//  DishDetailViewController.swift
//  RestaurantDemo
//
//  Created by Cristian Olteanu on 14/03/2018.
//  Copyright © 2018 Cristian Olteanu. All rights reserved.
//

import UIKit

class DishDetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var dishImageView: UIImageView!

    var dish: Dish!

    override func viewDidLoad() {
        super.viewDidLoad()

        nameLabel.text = dish.name
        dishImageView.image = dish.image
        descriptionTextView.text = dish.description
    }
}
