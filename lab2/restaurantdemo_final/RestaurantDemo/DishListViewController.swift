//
//  ViewController.swift
//  RestaurantDemo
//
//  Created by Cristian Olteanu on 14/03/2018.
//  Copyright © 2018 Cristian Olteanu. All rights reserved.
//

import UIKit
import SwiftRangeSlider

class DishListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let allSections = allDishSections()
    var filteredSections = allDishSections()

    @IBAction func sliderValueChanged(_ slider: RangeSlider) {
        let minPrice = Int(slider.lowerValue)
        let maxPrice = Int(slider.upperValue)

        let allSoups = allSections[0]
        let newSoups = allSoups.dishes.filter { dish -> Bool in
            if dish.price >= minPrice && dish.price <= maxPrice {
                return true
            } else {
                return false
            }
        }

        let allSolids = allSections[1]
        let newSolids = allSolids.dishes.filter { dish -> Bool in
            if dish.price >= minPrice && dish.price <= maxPrice {
                return true
            } else {
                return false
            }
        }

        filteredSections[0].dishes = newSoups
        filteredSections[1].dishes = newSolids

        tableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredSections.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return filteredSections[section].name
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredSections[section].dishes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dishSection = filteredSections[indexPath.section]
        let dish = dishSection.dishes[indexPath.row]
        
        let dishCell = tableView.dequeueReusableCell(withIdentifier: "dishCell") as! DishTableViewCell
        
        dishCell.nameLabel.text = dish.name
        dishCell.dishImageView.image = dish.image
        dishCell.priceLabel.text = "price: \(dish.price)"
        
        return dishCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let dishSection = filteredSections[indexPath.section]
        let selectedDish = dishSection.dishes[indexPath.row]

        let dishDetailViewController = storyboard?.instantiateViewController(withIdentifier: "dishDetail") as! DishDetailViewController
        dishDetailViewController.dish = selectedDish

        navigationController?.pushViewController(dishDetailViewController, animated: true)
    }

}

